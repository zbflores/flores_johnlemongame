﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WaypointPatrol : MonoBehaviour
{
    //Variables
    public NavMeshAgent navMeshAgent;
    public Transform[] waypoints;
    public GameObject player;
    public AudioClip otherClip;
    public AudioSource chaseMusic;
    public bool isChasing;
    public float aggro_distance = 8f;
    public float aggro_distance_escape = 12f;
    public float navStartSpeed;

    int m_CurrentWaypointIndex;
    //This function is called when the game is run and sets the destination of the ghosts to the waypoints they have.
    void Start()
    {
        navStartSpeed = navMeshAgent.speed;
        navMeshAgent.SetDestination(waypoints[0].position);
        isChasing = false;
    }
    //This function is called every frame and calculates the navMesh Destination
    void Update()
    {
        Vector3 dirTowardsPlayer = transform.position + player.transform.position;
        dirTowardsPlayer = dirTowardsPlayer.normalized;
        if (Vector3.Distance(transform.position, player.transform.position) < aggro_distance)
        {
            print("Player aggroed.");
            isChasing = true;
        }
        if (isChasing == false)
        {
            navMeshAgent.speed = navStartSpeed;
            if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)
            {
                m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;
                navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
            }
        }
        else if (isChasing == true)
        {
            navMeshAgent.speed = 5;
            navMeshAgent.SetDestination(player.transform.position);
            if (!chaseMusic.isPlaying)
            {
                chaseMusic.clip = otherClip;
                chaseMusic.Play();

            }
        }
        if (navMeshAgent.speed == 5 && Vector3.Distance(transform.position, player.transform.position) > aggro_distance_escape)
        {
          print("Chase ended.");
          navMeshAgent.speed = navStartSpeed;
          isChasing = false;
          chaseMusic.Stop();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;


public class PlayerMovement : MonoBehaviour
{
    //Variables
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    AudioSource m_AudioSource;
    Animator m_Animator;
    public Rigidbody m_Rigidbody;
    public AudioSource abilityUsed;
    public AudioSource abilityOver;
    public Slider health;
    public GameEnding gameEnding;
    public GameObject player;
    public GameObject food_Eaten;
    public CinemachineVirtualCamera virtualCamera;
    public bool invulnerable;
    public bool isGrounded = true;
    public float distToGround = 1f;
    public float m_Thrust = 1f;
    public float playerHealth;
    public float turnSpeed = 20f;
    private bool SpacePressed = false;
    float _slopeAngle;
    float invulnerableCooldown;
    float invulnerableSize;
    int count = 5;



    void Start()
    {
        playerHealth = 5f;
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
        CinemachineComponentBase componentBase = virtualCamera.GetCinemachineComponent(CinemachineCore.Stage.Body);
        if (componentBase is CinemachineFramingTransposer)
        {
            (componentBase as CinemachineFramingTransposer).m_CameraDistance = 16;
            virtualCamera.transform.Rotate(new Vector3(0, 0, 0));
        }
    }
    void FixedUpdate()
    {
        //Gets input from the player.
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);


        //Checks if the player is walking to play the footstep audio loop.
        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);

    }
    void Update()
    {
        SetSlider();
        GroundCheck();
        if (invulnerable)
        {
            invulnerableCooldown -= Time.deltaTime;
            print("invulnerabilityCooldown:" + invulnerableCooldown);
            if (invulnerableCooldown <= 0f)
            {
                invulnerable = false; //Invulnerability fades
                transform.localScale -= new Vector3(0.6f, 0.6f, 0.6f); //Decrease size
                abilityOver.Play();
            }
        }
        if (Input.GetKeyDown(KeyCode.E) && invulnerableCooldown <= 0f && food_Eaten.activeInHierarchy==false)
        {
            float invulnerableSize = 0.6f;
            transform.localScale += new Vector3(invulnerableSize, invulnerableSize, invulnerableSize);
            invulnerable = true; //Sets invulnerable as true
            invulnerableCooldown = 5f;
            abilityUsed.Play();

        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isGrounded == true)
            {
                //transform.Translate(Vector3.up * 100 * Time.deltaTime, Space.World);     Transform Jump
                if (food_Eaten.activeInHierarchy == false)
                {
                    m_Rigidbody.AddRelativeForce(Vector3.up * m_Thrust, ForceMode.Impulse);
                }
                else
                {
                    m_Rigidbody.AddRelativeForce(Vector3.up * 0.1f, ForceMode.Impulse);
                }    

            }

        }
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            if (isGrounded == true)
            {

            }



        }
        if (playerHealth <= 0)
        {
            gameEnding.CaughtPlayer();
        }



    }
    void GroundCheck()
    {

        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, distToGround + 0.0f))
        {
            _slopeAngle = (Vector3.Angle(hit.normal, transform.forward) - 90);
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }


    }
    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

    void SetSlider()
    {
        health.value = playerHealth;
    }
    public void LoseHealth()
    {
        playerHealth = playerHealth - 1;
    }


}




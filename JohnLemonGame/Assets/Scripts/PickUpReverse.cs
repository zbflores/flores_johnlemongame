﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PickUpReverse : MonoBehaviour
{
    public Rigidbody m_Rigidbody;
    public GameObject player;
    public GameObject upgrade_Object;
    public GameObject food_Eaten;
    public GameObject table_Spider;
    public GameObject spider_Activated;
    public Collider m_Upgrade;
    public CinemachineVirtualCamera virtualCamera;
    void Update()
    {
        upgrade_Object.transform.Rotate(new Vector3(0, 1, 0));

    }

    // Start is called before the first frame update
    void Start()
    {
        m_Upgrade = GetComponent<Collider>();
        m_Upgrade.isTrigger = true;
    }
    void OnTriggerEnter(Collider other)
    {
        //if (player.gameObject.CompareTag("Player"))
        if (food_Eaten.activeInHierarchy == true)
        {
            food_Eaten.SetActive(false);
            player.transform.localScale += new Vector3(0.87f, 0.87f, 0.87f);
            upgrade_Object.SetActive(false);
            Invoke("SpiderActive", 3);
            CinemachineComponentBase componentBase = virtualCamera.GetCinemachineComponent(CinemachineCore.Stage.Body);
            if (componentBase is CinemachineFramingTransposer)
            {
                (componentBase as CinemachineFramingTransposer).m_CameraDistance = 16;
                virtualCamera.transform.Rotate(new Vector3(32, 0, 0));
            }
        }


    }
    void SpiderActive()
    {
        table_Spider.GetComponent<WaypointPatrol>().enabled = true;
        spider_Activated.SetActive(true);
        print("Called from pickUp Reverse");
    }

}

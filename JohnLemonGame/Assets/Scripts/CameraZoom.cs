﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraZoom : MonoBehaviour
{

    void Update()
    {

        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            //GetComponent<CinemachineVirtualCamera>().m_Lens.OrthographicSize = ++;
            GetComponent<CinemachineVirtualCamera>().m_Lens.OrthographicSize = 5;
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            //GetComponent<CinemachineVirtualCamera>().m_Lens.OrthographicSize = --;
            GetComponent<CinemachineVirtualCamera>().m_Lens.OrthographicSize = 20;
        }
    }
}
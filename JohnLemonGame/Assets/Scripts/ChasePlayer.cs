﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasePlayer : MonoBehaviour
{
    public GameObject player;
    Rigidbody rigid_enemy;
    //speed of the enemy chasing player.
    public float speed;
    //distance that the enemy will aggro the player.
    public float aggro_distance = 10f;

    // Start is called before the first frame update
    private void Start()
    {
        rigid_enemy = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Observer : MonoBehaviour
{
// Variables section
    public Transform player;
    public GameEnding gameEnding;
// Boolean asking if player is in range.
    bool m_IsPlayerInRange;
//Looks for the transform of the player to collide with observer's point of view using IsTrigger.
    void OnTriggerEnter(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.transform == player)
        {
            m_IsPlayerInRange = false;
        }
    }

    void Update()
    {
        if (m_IsPlayerInRange)
        {
            Vector3 direction = player.position - transform.position + Vector3.up;
            Ray ray = new Ray(transform.position, direction);
            RaycastHit raycastHit;

            if (Physics.Raycast(ray, out raycastHit))
            {
                if (raycastHit.collider.transform == player)
                {
                    //EnemyMovement.AggroMechanic();
                    // Shows the player the game ending screen.
                    PlayerMovement pMovement = player.GetComponent<PlayerMovement>();
                    if (!pMovement.invulnerable)
                    {;
                        //LoseHealth.PlayerMovement();
                       gameEnding.CaughtPlayer();
                    }
                }
                else
                {
                    CancelInvoke("LoseHealth");
                }
            }
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateSpider : MonoBehaviour
{
    public GameObject spider_Activated;
    public Animator spider_Animator;
    bool already_done = false;
    bool isActive = false;
    void Start()
    {
        spider_Animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (spider_Activated.activeInHierarchy == true && already_done == false)
        {
            bool isActive = true;
            spider_Animator.SetBool("IsActive", isActive);
            already_done = true;
            print("Animation switched for spider.");
        }    
    }
}

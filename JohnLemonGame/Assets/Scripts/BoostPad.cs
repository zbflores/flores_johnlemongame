﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostPad : MonoBehaviour
{
    public float m_Boost = 10f;
    public Rigidbody m_Rigidbody;
    public GameObject player;
    public Collider m_BoostPad;
    // Start is called before the first frame update
    private void Start()
    {
        m_BoostPad = GetComponent<Collider>();
        m_BoostPad.isTrigger = true;
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
            print("Boost Detected Player");
            m_Rigidbody.AddRelativeForce(Vector3.up * m_Boost, ForceMode.Impulse);

    }
}

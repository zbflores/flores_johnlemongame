﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    public Rigidbody m_Rigidbody;
    public GameObject player;
    public Collider m_Teleporter;
    // Start is called before the first frame update
    private void Start()
    {
        m_Teleporter = GetComponent<Collider>();
        m_Teleporter.isTrigger = true;
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
            player.transform.position = new Vector3(47, 12, 62);

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PickUp : MonoBehaviour
{
    public Rigidbody m_Rigidbody;
    public GameObject player;
    public GameObject upgrade_Object;
    public GameObject food_Eaten;
    public Collider m_Upgrade;
    public CinemachineVirtualCamera virtualCamera;
    void Update()
    {
        upgrade_Object.transform.Rotate(new Vector3(0, 1, 0));

    }

    // Start is called before the first frame update
    private void Start()
    {
        m_Upgrade = GetComponent<Collider>();
        m_Upgrade.isTrigger = true;
    }
    void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.CompareTag("Player"))
            food_Eaten.SetActive(true);
            player.transform.localScale -= new Vector3(0.87f, 0.87f, 0.87f);
            upgrade_Object.SetActive(false);
            CinemachineComponentBase componentBase = virtualCamera.GetCinemachineComponent(CinemachineCore.Stage.Body);
            if (componentBase is CinemachineFramingTransposer)
            {
                (componentBase as CinemachineFramingTransposer).m_CameraDistance = 1;
                virtualCamera.transform.Rotate(new Vector3(-32, 0, 0));
            }


    }

}
